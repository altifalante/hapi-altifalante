const Altifalante = require('@altifalante/altifalante');

module.exports.register = function (server, options, cb) {
  const altifalanteInstance = Altifalante(options);

  Object.keys(options).forEach((eventName) => {
    server.ext(eventName, (server, next) => {
      altifalanteInstance.emit(eventName);
      return next();
    });
  });

  return cb();
};

module.exports.register.attributes = {
    pkg: require('../package.json')
};
