const Hapi = require('hapi');
const hapiAltifalante = require('../lib');

const server = new Hapi.Server();
server.connection({
  port: 5000
});

server.register({
  register: hapiAltifalante,
  options: {
    onPostStart: {
      notification: {
        message: 'Server Started'
      }
    },
    onRequest: {
      notification: {
        message: 'Request!'
      }
    },
  }
}, (err) => {
  server.route({
    method: 'GET',
    path: '/',
    handler: (request, reply) => {
      reply('Hello, world!');
    }
  });
  server.start();
});
